import Deck as d
import Player as p

class Board():

    def __init__(self, players):
        self.players = players
        self.board = []
        self.deck = d.Deck()
        self.currentPlayer = 0
        # Game prep
        self.fillHands()
        self.fillBoard()

    def play(self):
        play_info = self.players[self.currentPlayer].play()

        if True:
            self.currentPlayer = (self.currentPlayer+1) % 2

    def fillHands(self):
        for num in range(0, 3):
            self.players[0].hand.append(self.deck.draw())
            self.players[1].hand.append(self.deck.draw())

    def fillBoard(self):
        for num in range(0, 4):
            self.board.append(self.deck.draw())

    def reset(self):
        self.board = []
        self.deck = d.Deck()
        self.currentPlayer = 0
        self.fillHands()
        self.fillBoard()



if __name__ == "__main__":
    b = Board([p.Player("Ampo"), p.Player("Popcorn")])
    print(sorted(b.board))
