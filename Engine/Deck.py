import Card as c
import random

class Deck():
    deck = []
    def __init__(self):
        self.generateDeck()
        random.shuffle(self.deck)

    # Generates a standard deck
    def generateDeck(self):
        self.deck = []
        for family in c.FAMILIES:
            for num in range(1, 11):
                self.deck.append(c.Card(num, family))

    def draw(self):
        if len(self.deck) > 0:
            return self.deck.pop()
