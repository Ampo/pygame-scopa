'''
Card class
'''
FAMILIES = ['SW', 'CO', 'MA', 'CU']

class Card():

    def __init__(self, value, family):
        self.value = value
        if family in FAMILIES:
            self.family = family

    def __str__(self):
        return "Carte : "+str(self.value)+" "+str(self.family)

    def __repr__(self):
        return "Carte : "+str(self.value)+" "+str(self.family)

    def __gt__(self, card):
        val_s = 0
        val_c = 0
        if self.family == "CO":
            val_s += 11
        elif self.family == "MA":
            val_s += 22
        elif self.family == "CU":
            val_s += 33

        if card.family == "CO":
            val_c += 11
        elif card.family == "MA":
            val_c += 22
        elif card.family == "CU":
            val_c += 33

        val_s += self.value
        val_c += card.value

        return val_s > val_c
